\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}The Project}{1}
\contentsline {section}{\numberline {1.2}Objectives}{1}
\contentsline {section}{\numberline {1.3}Structure}{2}
\contentsline {chapter}{\numberline {2}Background}{3}
\contentsline {section}{\numberline {2.1}Mobile Application (Apps)}{3}
\contentsline {section}{\numberline {2.2}Route Card}{4}
\contentsline {section}{\numberline {2.3}Grid Reference Systems}{5}
\contentsline {section}{\numberline {2.4}Web Technologies}{5}
\contentsline {subsection}{\numberline {2.4.1}HTML5 \& CSS}{5}
\contentsline {subsection}{\numberline {2.4.2}JavaScript \& jQuery}{6}
\contentsline {subsection}{\numberline {2.4.3}PHP}{6}
\contentsline {section}{\numberline {2.5}Programming Languages}{6}
\contentsline {section}{\numberline {2.6}Data-Interchange Formats}{7}
\contentsline {subsection}{\numberline {2.6.1}XML}{7}
\contentsline {subsection}{\numberline {2.6.2}JSON}{7}
\contentsline {section}{\numberline {2.7}Web Service API's}{7}
\contentsline {subsection}{\numberline {2.7.1}Groovy \& Grails}{7}
\contentsline {subsection}{\numberline {2.7.2}Dropwizard}{8}
\contentsline {section}{\numberline {2.8}Build Tools}{8}
\contentsline {subsection}{\numberline {2.8.1}Jenkins}{8}
\contentsline {subsection}{\numberline {2.8.2}Redmine}{9}
\contentsline {subsection}{\numberline {2.8.3}GIT}{9}
\contentsline {subsection}{\numberline {2.8.4}Phonegap}{9}
\contentsline {section}{\numberline {2.9}Related Work}{10}
\contentsline {subsection}{\numberline {2.9.1}Enjoy Snowdonia App}{10}
\contentsline {chapter}{\numberline {3}Design}{11}
\contentsline {section}{\numberline {3.1}Methodology}{11}
\contentsline {subsection}{\numberline {3.1.1}Tool Selection Process}{11}
\contentsline {subsection}{\numberline {3.1.2}Main App Language}{11}
\contentsline {subsubsection}{MVC Design}{12}
\contentsline {paragraph}{}{12}
\contentsline {paragraph}{}{12}
\contentsline {paragraph}{}{13}
\contentsline {paragraph}{}{13}
\contentsline {subsubsection}{Native Theme}{13}
\contentsline {paragraph}{}{14}
\contentsline {subsubsection}{Mapping API}{14}
\contentsline {subsection}{\numberline {3.1.3}Data-Interchange Format}{15}
\contentsline {subsection}{\numberline {3.1.4}Backend Language}{16}
\contentsline {paragraph}{}{16}
\contentsline {section}{\numberline {3.2}Application Design}{16}
\contentsline {subsubsection}{End to End Process design}{16}
\contentsline {section}{\numberline {3.3}Backend Service Design }{16}
\contentsline {chapter}{\numberline {4}Implementation}{18}
\contentsline {section}{\numberline {4.1}Mobile Application Development}{18}
\contentsline {subsection}{\numberline {4.1.1}Core Application Development}{18}
\contentsline {subsection}{\numberline {4.1.2}Problems}{21}
\contentsline {subsubsection}{Latitude \& Longitude error}{21}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {paragraph}{}{23}
\contentsline {subsubsection}{digest Loop Error}{24}
\contentsline {paragraph}{}{24}
\contentsline {subsubsection}{Cross Origin Resource Sharing (CORS)}{25}
\contentsline {subsection}{\numberline {4.1.3}Specialised Version Development}{25}
\contentsline {paragraph}{}{25}
\contentsline {section}{\numberline {4.2}Backend Service Development}{26}
\contentsline {paragraph}{}{27}
\contentsline {chapter}{\numberline {5}Testing}{28}
\contentsline {section}{\numberline {5.1}Continuous Testing}{28}
\contentsline {paragraph}{}{28}
\contentsline {chapter}{\numberline {6}Evaluation}{30}
\contentsline {section}{\numberline {6.1}User Feedback Questionnaire}{30}
\contentsline {section}{\numberline {6.2}Improving App Speed}{31}
\contentsline {section}{\numberline {6.3}Error Reporting}{31}
\contentsline {section}{\numberline {6.4}Backend Implementation Evaluation}{32}
\contentsline {section}{\numberline {6.5}Social, legal and Ethical Issues}{32}
\contentsline {paragraph}{}{32}
\contentsline {section}{\numberline {6.6}Known Issues}{33}
\contentsline {chapter}{\numberline {7}Conclusion}{34}
\contentsline {section}{\numberline {7.1}Future Work}{34}
\contentsline {section}{\numberline {7.2}Overall Review of Application}{34}
\contentsline {chapter}{Bibliography}{36}
\contentsline {chapter}{\numberline {8}Appendix}{37}
\contentsline {section}{\numberline {8.1}OS Detection Code}{40}
\contentsline {section}{\numberline {8.2}jQT Bootstrap Code}{43}
\contentsline {section}{\numberline {8.3}Controllers.js}{46}
\contentsline {section}{\numberline {8.4}Services.js}{50}
\contentsline {section}{\numberline {8.5}\_952HillsafeApp.js}{59}
\contentsline {section}{\numberline {8.6}Index.html}{64}
\contentsline {section}{\numberline {8.7}Route Card Class}{76}
